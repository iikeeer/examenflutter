import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App'),
        ),
        body: Center(
          child: Container(
            decoration: BoxDecoration(color: Colors.amber),
            child: Contador(),
          ),
        ),
      ),
    );
  }
}

class Contador extends StatefulWidget {
  Contador({Key key}) : super(key: key);

  @override
  _ContadorState createState() => _ContadorState();
}

class _ContadorState extends State<Contador> {
  int contador = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 100,
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FloatingActionButton(
                onPressed: _sumar,
                child: Icon(
                  Icons.add,
                  size: 35,
                  color: Colors.black,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: Text(
                  "$contador",
                  style: TextStyle(
                    fontSize: 40,
                  ),
                ),
              ),
              FloatingActionButton(
                onPressed: _restar,
                child: Icon(
                  Icons.remove,
                  size: 35,
                  color: Colors.black,
                ),
              ),
            ],
          ),
        ),
        Container(
          color: Colors.grey[300],
          height: 503.4,
          child: GridView.count(
            padding: EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 4,
            children: [
              for (var i = 1; i < contador + 1; i++)
                i % 3 == 0 && i % 5 == 0
                    ? Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(8),
                        child: Text('FB'),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.bottomLeft,
                            colors: [Colors.blue, Colors.green],
                          ),
                        ),
                      )
                    : i % 5 == 0
                        ? Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(8),
                            child: Text('Buck'),
                            color: Colors.green,
                          )
                        : i % 3 == 0
                            ? Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(8),
                                child: Text('Face'),
                                color: Colors.blue,
                              )
                            : Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(8),
                                child: Text('$i'),
                                color: Colors.white,
                              )
            ],
          ),
        ),
      ],
    );
  }

  void _sumar() {
    if (contador < 33) {
      setState(() => contador++);
    } else {}
  }

  void _restar() {
    if (contador > 0) {
      setState(() => contador--);
    } else {}
  }
}
